import game.GuessingGame;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String numeros = "";
        int one, two, three, four;
        boolean ganhou = false;
        GuessingGame Gg = new GuessingGame();

        Gg.printNumber();
        do {
            do {
                System.out.print("Digite o 1º numero: ");
                one = s.nextInt();
            } while (one < 0 || one >= 10);
            do {
                System.out.print("Digite o 2º numero: ");
                two = s.nextInt();
            } while (two < 0 || two >= 10);
            do {
                System.out.print("Digite o 3º numero: ");
                three = s.nextInt();
            } while (three < 0 || three >= 10);
            do {
                System.out.print("Digite o 4º numero: ");
                four = s.nextInt();
            } while (four < 0 || four >= 10);
                ganhou = Gg.tryNumbers(one, two, three, four);
                Gg.printAttempt();
        } while (!ganhou);
        Gg.printWin();

    }

}
