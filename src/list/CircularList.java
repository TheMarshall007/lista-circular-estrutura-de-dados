package list;

import java.util.ArrayList;

public class CircularList<T> {

    private Nodo<T> first;
    private Nodo<T> last;
    private int nodoAmount;

    public void insert(T element) {
        if (isEmpty()) {
            this.first = new Nodo(element);
            this.last = this.first;
            this.first.setNext(last);
            this.last.setPrevious(first);
            nodoAmount++;
        } else {
            Nodo newNodo = new Nodo(element);
            this.last.setNext(newNodo);
            newNodo.setPrevious(this.last);
            this.last = newNodo;
            this.last.setNext(first);
            this.first.setPrevious(last);
            nodoAmount++;
        }
    }

    public void removeElement(T element) {
        Nodo nodo = this.first.getNext();
        boolean removed = false;
        for (int i = 0; i < nodoAmount; i++) {
            if (element == nodo.getElement()) {
                Nodo previous = nodo.getPrevious();
                Nodo next = nodo.getNext();
                previous.setNext(next);
                next.setPrevious(previous);
                nodoAmount--;
                removed = true;
            }
            nodo = nodo.getNext();
            if (removed) {
                i = nodoAmount;
            }
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    public Nodo<T> getFirst() {
        return first;
    }

    public Nodo<T> getLast() {
        return last;
    }

    public int getNodoAmount() {
        return nodoAmount;
    }

    public boolean elementInList(Object element) {
        Nodo nodo = first;
        for (int i = 0; i < nodoAmount; i++) {
            if (nodo.getElement() == element) {
                return true;
            }
            nodo = nodo.getNext();
        }
        return false;
    }

    public Object getElementByPosition(int position){
        if(position >= 0 && position < nodoAmount){
            Nodo nodo = this.first;
            for (int i = 0; i <= position; i++){
                if (i == position){
                    return nodo.getElement();
                }
                nodo = nodo.getNext();
            }
        }
        return null;
    }

    public void showNodoListFirstToLast() {
        Nodo nodo = first;
        System.out.print("[ ");
        for (int i = 0; i < nodoAmount; i++) {
            System.out.print(nodo.getElement());
            if (i + 1 != nodoAmount) {
                System.out.print(" | ");
            }
            nodo = nodo.getNext();
        }
        System.out.println(" ]");
    }

}