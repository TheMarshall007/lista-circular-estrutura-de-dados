package game;

import list.CircularList;

public class PlayerAttempt {
    private int attempt;
    private CircularList<Integer> numbers = new CircularList<>();
    private int numberOfCorrectDigits;
    private int numberOfCorrectDigitsInCorrectPositions;

    public void addNumbers(int attempt, int one, int two, int three, int four) {
        this.attempt = attempt;
        numbers.insert(one);
        numbers.insert(two);
        numbers.insert(three);
        numbers.insert(four);
    }

    public int getAttempt() {
        return attempt;
    }

    public CircularList<Integer> getNumbers() {
        return numbers;
    }

    public int getNumberOfCorrectDigits() {
        return numberOfCorrectDigits;
    }

    public void addNumberOfCorrectDigits() {
        this.numberOfCorrectDigits ++;
    }

    public int getNumberOfCorrectDigitsInCorrectPositions() {
        return numberOfCorrectDigitsInCorrectPositions;
    }

    public int addNumberOfCorrectDigitsInCorrectPositions() {
        return numberOfCorrectDigitsInCorrectPositions ++;
    }

    public void setNumberOfCorrectDigitsInCorrectPositions(int numberOfCorrectDigitsInCorrectPositions) {
        this.numberOfCorrectDigitsInCorrectPositions = numberOfCorrectDigitsInCorrectPositions;
    }


    public void printAttempt() {
        System.out.println(attempt +"º tentativa: " +numbers.getElementByPosition(0) +
                numbers.getElementByPosition(1)+
                        numbers.getElementByPosition(2)+
                        numbers.getElementByPosition(3)+
                "    { número de dígitos corretos: " + numberOfCorrectDigits + "}" +
                "\n                      " +
                "{ número de dígitos corretos em posições corretas: " + numberOfCorrectDigitsInCorrectPositions + "}");
    }
}
