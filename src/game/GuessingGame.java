package game;

import list.CircularList;

import java.util.Random;

public class GuessingGame<T> {
    private CircularList<Integer> number = new CircularList<>();
    private CircularList<PlayerAttempt> attempts = new CircularList<>();
    private int triedAmount;

    public GuessingGame() {
        Random r = new Random();
        number.insert(r.nextInt(10));
        number.insert(r.nextInt(10));
        number.insert(r.nextInt(10));
        number.insert(r.nextInt(10));
    }

    public boolean tryNumbers(int one, int two, int three, int four) {
        PlayerAttempt attempt = new PlayerAttempt();
        triedAmount++;
        attempt.addNumbers(triedAmount, one, two, three, four);
        attempts.insert(attempt);

        verifyCorrectPositions(attempt);
        verifyCorrectNumbers(attempt);
        if (attempt.getNumberOfCorrectDigits() == 4 && attempt.getNumberOfCorrectDigitsInCorrectPositions() == 4){
            return true;
        }
        return false;
    }

    private void verifyCorrectPositions(PlayerAttempt attempt) {
        for (int i = 0; i < 4; i++) {
            if (number.getElementByPosition(i) == attempt.getNumbers().getElementByPosition(i)) {
                attempt.addNumberOfCorrectDigitsInCorrectPositions();
            }
        }
    }

    private void verifyCorrectNumbers(PlayerAttempt attempt) {
        CircularList<T> numberAttemptList = new CircularList<>();
        int numberOfRepeated = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = i + 1; j < 4; j++) {
                if (number.getElementByPosition(i) == number.getElementByPosition(j)) {
                    numberOfRepeated++;
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            if(number.elementInList(attempt.getNumbers().getElementByPosition(i))) {
                if (!numberAttemptList.elementInList(attempt.getNumbers().getElementByPosition(i))) {
                    numberAttemptList.insert((T)attempt.getNumbers().getElementByPosition(i));
                    attempt.addNumberOfCorrectDigits();
                } else if (numberOfRepeated != 0) {
                    attempt.addNumberOfCorrectDigits();
                    numberOfRepeated--;
                }
            }
        }
    }

    public void printNumber() {
        System.out.println(number.getElementByPosition(0)+""
                + number.getElementByPosition(1)
                +"" +number.getElementByPosition(2)
                +"" +number.getElementByPosition(3)
        );
    }

    public void printAttempt(){
        attempts.getLast().getElement().printAttempt();
    }

    public void printWin(){
        System.out.println("======Você acertou!!======");
    }
}
